# wikilite 

Minimalistic wiki for local deployments.

Uses sqlite for storage (no versioning), python's default `http.server` for serving the contents and [text/gemini](https://gemini.circumlunar.space/docs/specification.html) as its markup language, using only features from the standard library.

Capabilities:

- Editing of page: By visiting a nonexisting page, by appending the query parameter `?edit` or by hitting the edit button at the end of each page.
- Search with the special endpoint `.search`: `/.search?my+search+parameters`
- Adding custom css styles with the endpoint `/.css?edit`
- Dumping the current state of the database as sqlite-sql with the endpoints `/.dump` and `/.dump.gz` for a compressed dump

# notes

- This is a tool for local deployments in trusted environments
- This uses `http.server` to serve the webpage
- There is no protection against XSS and similar attacks
- There is only limited protection against SQL injection 
- Pages are not versioned
- `-` and `*` are both supported for denoting a list element in contrast to the `text/gemini` specification

