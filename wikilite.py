#!/usr/bin/env python3

import sqlite3
import re
import http.server
import html
import urllib.parse
import email
import gzip

from functools import partial

from argparse import ArgumentParser

LINK_LINE_RE = re.compile(r"^=>\s*(?P<url>\S+)(\s+(?P<text>.*))?$")
HEADLINE_LINE_RE = re.compile(r"^(?P<level>#{1,3}) (?P<text>.*)$")
LIST_LINE_RE = re.compile(r"^[*-] (?P<text>.*)$")
BLOCK_QUOTE_RE = re.compile(r"^> (?P<text>.*)$")
PRE_RE = re.compile(r"^```$")

EDIT_PAGE = """
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<link rel="stylesheet" href="/.css">
<title>{name}</title>
</head>
<body>
<header>
<h1>{name}</h1>
</header>
<main>
<form action='{name}' method="post">
<textarea name="content" rows="20" cols="80">{content}</textarea></br>
<input type="submit" value="Submit"/>
</form>
</main>
</body>
</html>
"""

class Repo():

  def get_by_name(self, name):
    raise Exception()

  def put_by_name(self, name, content):
    raise Exception()

  def dump(self):
    raise Exception()

  def search(self, query):
    raise Exception()

  def get_stylesheet(self):
    raise Exception()

  def set_stylesheet(self, stylesheet):
    raise Exception()

class SqliteRepo(Repo):

  def __init__(self, path):
    self.connection = sqlite3.connect(path)
    self.init()

  def init(self):
    c = self.connection.cursor()
    c.execute("CREATE TABLE IF NOT EXISTS content (name TEXT PRIMARY KEY, content TEXT)")
    c.execute("CREATE TABLE IF NOT EXISTS config (key TEXT PRIMARY KEY, value TEXT)")
    self.connection.commit()

  def get_by_name(self, name):
    c = self.connection.cursor()
    c.execute("SELECT content FROM content WHERE name = ?", (name,))
    row = c.fetchone()
    return row[0] if row is not None else None
    
  def put_by_name(self, name, content):
    c = self.connection.cursor()
    c.execute("REPLACE INTO content (name, content) VALUES (?, ?)", (name, content))
    self.connection.commit()

  def dump(self):
    return '\n'.join(self.connection.iterdump())

  def search(self, query):
    c = self.connection.cursor()
    return list(c.execute("SELECT name FROM content WHERE name LIKE '%' || ? || '%' OR content LIKE '%' || ? || '%' LIMIT 25", (query, query)))

  def get_stylesheet(self):
    c = self.connection.cursor()
    c.execute("SELECT value FROM config WHERE key = 'stylesheet'")
    row = c.fetchone()
    return row[0] if row is not None else None
    
  def set_stylesheet(self, stylesheet):
    c = self.connection.cursor()
    c.execute("REPLACE INTO config (key, value) VALUES ('stylesheet', ?)", (stylesheet,))
    self.connection.commit()

class GeminiParser():

  def __init__(self, text):
    self.text = text.__iter__()

  def parsed_lines(self):
    for line in self.text:
      line = line.rstrip()

      if (match := LINK_LINE_RE.fullmatch(line)) is not None:
        yield {
          "type": "link",
          "url": match.group("url"),
          "text": match.group("text"),
          "raw": line
        }
      elif (match := HEADLINE_LINE_RE.fullmatch(line)) is not None:
        yield {
          "type": "headline",
          "level": len(match.group("level")),
          "text": match.group("text"),
          "raw": line
        }
      elif (match := LIST_LINE_RE.fullmatch(line)) is not None:
        yield {
          "type": "list",
          "text": match.group("text"),
          "raw": line
        }
      elif (match := BLOCK_QUOTE_RE.fullmatch(line)) is not None:
        yield {
          "type": "block_quote",
          "text": match.group("text"),
          "raw": line
        }
      elif (match := PRE_RE.fullmatch(line)) is not None:
        yield {
          "type": "pre",
          "raw": line
        }
      else:
        yield {
          "type": "text",
          "text": line,
          "raw": line
        }

def translate_gemini_to_html(source):
  p = GeminiParser(source)

  state = "norm"

  for token in p.parsed_lines():
    processed = False
    while not processed:
      processed = True

      if token.get('text') is not None:
        text = html.escape(token['text'])
      else:
        text = None

      if state == "norm":
        if token["type"] == "headline":
          yield(f"<h{token['level']}>{text}</h{token['level']}>")
        elif token["type"] == "text":
          if len(text) == 0:
            yield("")
          else:
            yield(f"<p>{text}</p>")
        elif token["type"] == "block_quote":
          yield(f'<blockquote>{text}</blockquote>')
        elif token["type"] == "link":
          text = text if text is not None else token["url"]
          yield(f'<p><a href="{token["url"]}">{text}</a></p>')
        elif token["type"] == "list":
          yield("<ul>")
          state = "list"
          processed = False
        elif token["type"] == "pre":
          yield("<pre>")
          state = "pre"

      elif state == "list":
        if token["type"] != "list":
          yield("</ul>")
          state = "norm"
          processed = False
        else:
          yield(f" <li>{text}</li>")

      elif state == "pre":
        if token["type"] == "pre":
          yield("</pre>")
          state = "norm"
        else:
          yield(token["raw"])

  if state == "list":
    yield("</ul>")
  elif state == "pre":
    yield("</pre>")

def render_page(source, title=None):
  
  elements = []

  elements.append('<!DOCTYPE html>')
  elements.append('<html>')
  elements.append('<head>')
  elements.append('<meta charset="utf-8"/>')
  elements.append('<link rel="stylesheet" href="/.css">')
  if title is not None:
    elements.append(f'<title>{title}</title>')
  elements.append('</head>')

  elements.append('<body>')
  elements.append('<main>')
  elements.append('<article>')
  elements.extend(translate_gemini_to_html(source))
  elements.append('</article>')
  elements.append('</main>')
  elements.append('<footer><a href="?edit">edit</footer></a>')
  elements.append('</body>')
  elements.append('</html>')

  return '\n'.join(elements)

def render_edit_page(source, title):
  source = source if source is not None else ""
  return EDIT_PAGE.format(content = source, name = title)

def render_search_results(results, query):
  query = html.escape(query)
  elements = []

  elements.append('<!DOCTYPE html>')
  elements.append('<html>')
  elements.append('<head>')
  elements.append('<meta charset="utf-8"/>')
  elements.append('<link rel="stylesheet" href="/.css">')
  elements.append(f'<title>Search results: {query}</title>')
  elements.append('</head>')

  elements.append('<body>')
  elements.append(f'<header><h1>Search results for: <q>{query}</q></h1></header>')
  elements.append('<main><ul>')
  elements.append('<ul>')
  for result in results:
    file_name = result[0]
    elements.append(f'<li><a href="{file_name}">{file_name}</a></li>')
  elements.append('</ul>')
  elements.append('</main>')
  elements.append('</body>')
  elements.append('</html>')

  return '\n'.join(elements)
  

class Handler(http.server.BaseHTTPRequestHandler):

  def __init__(self, *args, repo=None, **kwargs):
    self.repo = repo if repo is not None else SqliteRepo(":memory:") 
    super().__init__(*args, **kwargs)

  def do_HEAD(self):
    self.send_header()

  def do_GET(self):
    if (content := self.send_head()) is not None:
      self.wfile.write(content)

  def send_head(self):

    if self.path == '/favicon.ico':
      self.send_error(http.server.HTTPStatus.NOT_FOUND, "File not found")
      return None

    query_split = self.path.split('?', 1)
    file_path = query_split[0]
    file_path = file_path.split('#',1)[0]

    if len(query_split) == 2:
      query = self.path.split('?', 1)[1]
      query = query.split('#',1)[0]
    else:
      query = None

    if file_path == "/.dump":
      content_type = "text/plain"
      content = self.repo.dump()
    elif file_path == "/.dump.gz":
      content_type = "application/gzip"
      content = gzip.compress(self.repo.dump().encode("UTF-8"))
    elif file_path == "/.search":
      content_type = "text/html"
      results = self.repo.search(query)
      content = render_search_results(results, query)
    elif file_path == "/.css":
      stylesheet = self.repo.get_stylesheet()
      if query == 'edit':
        content_type = "text/html"
        content = render_edit_page(stylesheet, "/.css")
      else:
        content_type = "text/css"
        content = stylesheet if stylesheet is not None else ""
    else:
      content_type = "text/html"
      gmi_content = self.repo.get_by_name(file_path)

      if query == 'edit' or gmi_content is None:
        content = render_edit_page(gmi_content, file_path)
      else:
        content = render_page(gmi_content.splitlines(), title = file_path)

    if content_type.startswith('text/'):
        content = content.encode("UTF-8")

    self.send_response(http.server.HTTPStatus.OK)
    self.send_header("Content-type", content_type)
    self.send_header("Content-Length", str(len(content)))
    self.end_headers()
    return content

  def do_POST(self):
    file_path = self.path.split('?', 1)[0]
    file_path = file_path.split('#', 1)[0]

    content = self.rfile.read(int(self.headers['Content-Length'])).decode("UTF-8")

    content = content.split('=',1)
    if content[0] == 'content':
      content = urllib.parse.unquote_plus(content[1])
      if file_path == '/.css':
        self.repo.set_stylesheet(content)
      else:
        self.repo.put_by_name(file_path, content)

      self.send_response(http.server.HTTPStatus.SEE_OTHER)
      self.send_header("Location", file_path)
      self.end_headers()
    else:
      # todo send right content
      self.send_error(http.server.HTTPStatus.NOT_FOUND, "File not found")

def run(port=3125, address='', server_class=http.server.HTTPServer, handler_class=http.server.BaseHTTPRequestHandler):
    server_address = (address, port)
    httpd = server_class(server_address, handler_class)
    print(f"serving on {address}:{port}")
    httpd.serve_forever()


parser = ArgumentParser()
parser.add_argument("--port", "-p", default="3125", type=int)
parser.add_argument("--host", "-H", default="127.0.0.1")
parser.add_argument("repo", default=":memory:", nargs="?")

args = parser.parse_args()

run(
  port = args.port,
  address = args.host,
  handler_class = partial(Handler, repo=SqliteRepo(args.repo))
)

